const conn = require('./mongoDB')

const testDatabase = () => {
  conn.then((client) => {
    if (!client) {
      console.log("X Database - Error")
      client.close()
    }
    console.log("+ Database - Connected")
    client.close()
  })
}

testDatabase()