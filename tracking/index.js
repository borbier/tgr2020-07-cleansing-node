const MongoClient = require('mongodb').MongoClient
const dotenv = require('dotenv')

dotenv.config()
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

const cleanedQuery = {
  sensor_type: "track",
  cleaned: {
    '$ne': true
  }
}

const checkEvent = (event_code) => {
  if (event_code === 8) {
    return "static"
  } else if (event_code === 128) {
    return "falling"
  } else if (event_code === 255) {
    return "sliding"
  } else {
    return "NaE"
  }
}

async function cleansingPM25() {
  console.log("Cleansing pm25 data")

  MongoClient.connect("mongodb://202.139.192.177:27017/07sensor", options, (err, db) => {
    if (err) throw err
    const dbo = db.db("07sensor")

    let bulk = dbo.collection("track").initializeOrderedBulkOp()
    dbo.collection("raw-data").find(cleanedQuery).sort({ 'ts': -1 }).toArray((err, result) => {
      if (err) throw err
      for (let i = 0; i < result.length; i++) {
        const rawPayload = result[i]
        const _id = rawPayload._id
        if (i === 0) {
          firstDocTs = rawPayload.ts
        } else if (i == result.length - 1) {
          lastDocTs = rawPayload.ts
        }
        const event_code = checkEvent(rawPayload.data.event_code)

        const payload = {
          sensor_id: rawPayload.sensor_id,
          mac_addr: rawPayload.data.mac_addr,
          rssi: rawPayload.data.rssi,
          event_code: event_code,
          ts: rawPayload.ts
        }
        
        bulk.insert(payload)
      }
    })

    bulk.execute((err, res) => {
      console.log("Inserted bulk ! Yay!")
    })
  })
}

setInterval(cleansingPM25, 10000);
