const MongoClient = require('mongodb').MongoClient
const dotenv = require('dotenv')

dotenv.config()
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}
const cleanedQuery = {
  sensor_type: "pm25",
  cleaned: {
    $ne: true
  }
}

async function cleansingPM25() {
  console.log("Cleansing pm25 data ", Date().toString())

  MongoClient.connect("mongodb://202.139.192.177:27017/07sensor", options, (err, db) => {
    if (err) throw err
    const dbo = db.db("07sensor")
    let bulk = dbo.collection("track").initializeOrderedBulkOp()

    dbo.collection("raw-data").find(cleanedQuery).limit(100).toArray((err, result) => {
      if (err) throw err
      for (let i = 0; i < result.length; i++) {
        let payload = null
        const rawPayload = result[i]
        const _id = rawPayload._id
        try {
          payload = {
            device_id: rawPayload.sensor_id,
            lat: rawPayload.data.LrrLAT,
            lon: rawPayload.data.LrrLON,
            value: parseInt("0x" + rawPayload.data.payload_hex.substring(2, 4)),
            ts: rawPayload.ts
          }
          if (payload.device_id >= 44 && payload.device_id <= 47 && (payload.value !== 0 || payload.value !== 191)) {
            bulk.insert(payload)
          }
        } catch (e) {
          console.log("Cannot parse from raw data format")
        }
      }
    })

    bulk.execute((err, res) => {
      if (err) console.log(err)
      console.log("Inserted bulk ! Yay!")
    })
  })
}

setInterval(cleansingPM25, 15000);